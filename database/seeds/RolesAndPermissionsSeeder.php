<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $adminColumnName = 'super-admin';
    public function run()
    {
       // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

       // create permissions array
        $arrayOfPermissionNames = [
            'test.create',
            'test.view',
            'test.update',
            'test.delete',
            'test.search',
            'realizar_test.create',
            'realizar_test.view',
            'realizar_test.search',
            'resultados_test.view',
            'permission.create',
            'permission.view',
            'permission.update',
            'permission.delete',
            'permission.search',
            'roles.create',
            'roles.view',
            'roles.update',
            'roles.delete',
            'roles.search',
            'roles.permission',
            'users.create',
            'users.update',
            'users.delete',
            'users.search', 
            'users.roles',
            'users.view'           
        ];

        // create permissions
        $permissions = $this->collectArray($arrayOfPermissionNames);
        Permission::insert($permissions->toArray());

        // create roles array by default
        $arrayOfRolesNames = [
            $this->adminColumnName,
            'admin',
            'docente',
            'alumno'
        ];

        if($arrayOfRolesNames[0])
        {
            Role::create(['name' => $arrayOfRolesNames[0]])
                ->givePermissionTo(Permission::all());
                
            $user = User::findOrFail(1);
            $user->assignRole([$this->adminColumnName]);
            //$user->givePermissionTo(Permission::all());
        }
        Role::insert($this->collectArray($this->remove_admin($arrayOfRolesNames))->toArray());
        $roleAdmin      = Role::findOrFail(2)->givePermissionTo($arrayOfPermissionNames[0],$arrayOfPermissionNames[1],$arrayOfPermissionNames[2],$arrayOfPermissionNames[3],$arrayOfPermissionNames[4],$arrayOfPermissionNames[15],$arrayOfPermissionNames[20],$arrayOfPermissionNames[21],$arrayOfPermissionNames[22],$arrayOfPermissionNames[23],$arrayOfPermissionNames[24],$arrayOfPermissionNames[25]);
        $roleDocente    = Role::findOrFail(3)->givePermissionTo($arrayOfPermissionNames[0],$arrayOfPermissionNames[1],$arrayOfPermissionNames[2],$arrayOfPermissionNames[3],$arrayOfPermissionNames[4],$arrayOfPermissionNames[8]);
        $roleEstudiante = Role::findOrFail(4)->givePermissionTo($arrayOfPermissionNames[5],$arrayOfPermissionNames[6],$arrayOfPermissionNames[7]);
    }

    protected function collectArray($array)
    {
        return collect($array)->map(function ($name) {
            return ['name' => $name, 'guard_name' => 'web'];
        });
    }

    protected function remove_admin($array = [])
    {
        array_shift($array);
        return $array;
    }
}
