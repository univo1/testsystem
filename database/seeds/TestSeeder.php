<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //nando
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Ciencia Basica","codigo" => "cie10021635","fecha_inicio" => "2020-10-09 00:00:00","fecha_fin" =>"2020-10-11 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Lenguaje y Literatura","codigo" => "len10022555","fecha_inicio" => "2020-10-09 00:00:00","fecha_fin" =>"2020-10-11 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Historia","codigo" => "his10031555","fecha_inicio" => "2020-10-09 00:00:00","fecha_fin" =>"2020-10-11 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Geografia","codigo" => "geo10041795","fecha_inicio" => "2020-10-09 00:00:00","fecha_fin" =>"2020-10-11 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Geografia_2","codigo" => "geo10051985","fecha_inicio" => "2020-10-09 00:00:00","fecha_fin" =>"2020-10-11 00:00:00"]);
        //celestino
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Lenguaje Y Literatura 2","codigo" => "len10068695","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Estudios Sociales","codigo" => "est12100635","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Matemáticas","codigo" => "mat12221635","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Ciencias Naturales","codigo" => "cie13421645","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Cultura General","codigo" => "cul15521545","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Geografia Básica","codigo" => "geo16021666","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
        DB::table('tests')->insert(["user_id"=>1,"name"=>"Historia 2","codigo" => "his17821644","fecha_inicio" => "2020-10-11 00:00:00","fecha_fin" =>"2020-10-13 00:00:00"]);
    }
}
