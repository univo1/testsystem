<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ModuloV</title>
    <!-- Scripts 
    <script src="{{ asset('js/app.js') }}" defer></script> -->
    <!-- Bootstrap CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> 
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,700&display=swap" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/c7999f3602.js" crossorigin="anonymous"></script>
    <!-- Sweetalert2 Styles -->
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>
      <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
          <a class="navbar-brand" href="#">StudyTest</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        
          <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="fas fa-align-justify"></i>
                  Pruebas
                </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">
                    <i class="far fa-bell"></i>
                    Notificaciones
                  </a>
              </li>
              <li class="nav-item">
                  <div class="dropdown">
                      <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-user"></i>
                        Mi cuenta
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">
                          <i class="far fa-address-card"></i>
                          Perfil
                        </button>
                        <button class="dropdown-item" type="button">
                          <i class="fas fa-book"></i>
                          Calificaciones
                        </button>
                        <button class="dropdown-item" type="button">
                          <i class="fas fa-sign-out-alt"></i>
                          Cerrar sesión
                        </button>
                      </div>
                  </div>    
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Buscar">
              <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </form>
          </div>
      </nav>
        <div id="contenido" class="container bg-grey w-100">
            <div class="card text-center mt-5">
                <div class="card-header">
                  Prueba de CIENCIA BASICA
                </div>
                <div class="card-body">
                    <h6 class="card-text">4. ¿Un microgramo equivale a 0,001 gramos?</h6>
                    <p>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label" for="inlineRadio1">Verdadero</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label" for="inlineRadio2">Falso</label>
                          </div>
                    </p>
                  <a href="{{ asset('/preguntacinco') }}" class="btn btn-primary float-right">Siguiente</a>
                </div>
                <div class="card-footer text-muted">
                  4 de 10
                </div>
              </div>
        </div> 
        <nav class="navbar navbar-expand-lg navbar-light fixed-bottom justify-content-center" style="background-color: #c7e3f7;">
          Copyright 
          &nbsp <i class="fa fa-copyright" aria-hidden="true"></i>&nbsp 
          ModuloV
        </nav>
      </div>
      <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
      <script src="{{ asset('js/popper.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/vue.js') }}"></script>
      <script src="{{ asset('js/axios.js') }}"></script>
      @yield('scripts')
</body>
</html>
