<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ModuloV</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script> 
    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,700&display=swap" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/c7999f3602.js" crossorigin="anonymous"></script>
    <!-- Sweetalert2 Styles -->
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
    <style>
        .efectoLateral{
            margin-left: 5px;
            width: 100%;
            transition: background-color .8s;
        }
        .efectoLateral:hover{
            background-color: #263BA1;
        }
        .efecto{
            background-color: #EAEAEA;
            padding-left: 30px;
            margin-bottom: 5px;
            border-radius: 5px;
            transition: background-color .8s;
        }
        .efecto:hover{
            padding-left: 85px;
            background-color: #9FA6D4;
        }
        .centrar{
            margin-top: auto;
            margin-bottom: auto;
        }
        .color{
            background-color: red;
        }
        :root {
            --primary: #111B54;
            --light: #ffffff;
            --grey: #efefef;
        }

        body {
            overflow: hidden;
            font-family: 'Muli', sans-serif;
            font-weight: 300;
            color: var(--primary);
        }
        a:hover {
            text-decoration: none;
        }
        .bg-primary { background-color: var(--primary) !important; }
        .btn-primary {
            background-color: var(--primary);
            border: 0;
        }
        .btn-primary:hover {
            background-color: var(--primary);
        }
        #navbar {
            min-height: 100vh;
        }
        #navbar .logo{
            padding: 0.875rem 1.25rem;
        }
        
        #navbar .menu {
            width: 12rem;
        }

        #content {
            overflow-y: auto;
            height: 100vh;
            padding-top: 2rem;
            padding-bottom: 5rem;
        }

        .btn-search {
            right: 0;
        }

        .stat {
            border-right: 1px solid var(--grey);
        } 
        @media screen and (max-width: 993px)  {
            #navbar {
            display: none;
            }
            .stat {
                border: 0;
            }
            .ocultarCerrarSesion{
                display: none;
            }
        }
        @media screen and (min-width: 992px){
            .ocultar{
                display: none;
            }
            .efecto{
                padding-left: 0px;
                margin-bottom: 0px;
                border-radius: 0px;
            }
        }
    </style>
    @yield('styles')
</head>
<body>
    <div id="app">
        <div class="d-flex" id="content-wrapper">
            <!-- Barra lateral sidebar -->
            @auth
                <div id="navbar" class="bg-primary">
                    @hasrole('super-admin')
                        <div class="logo">
                            <h4 class="text-light font-weight-bold mb-0">
                                <a href="{{ route('admin.index') }}" class="navbar-brand">TestSystem</a>    
                            </h4> 
                        </div>
                    @else
                        <div class="logo">
                            <h4 class="text-light font-weight-bold mb-0">
                                <a href="#" class="navbar-brand">TestSystem</a>
                            </h4> 
                        </div>
                    @endhasrole
                    @hasanyrole('super-admin|admin|superadmin')
                        <div class="menu">
                            <a href="{{ asset('/admin') }}" class="d-block text-light p-3 border-0 efectoLateral">
                                <i class="fas fa-home"></i>
                                Home
                            </a>
                        </div>
                    @endhasanyrole
                        <div class="menu">
                            <a href="{{ asset('/test') }}" class="d-block text-light p-3 border-0 efectoLateral">
                                <i class="fas fa-tasks"></i>
                                Test
                            </a>
                        </div>
                </div>
            @endauth
            <!-- Fin de barra lateral sidebar -->
            <div class="w-100">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <div class="container">
                    <button class="navbar-toggler" type="button" style="margin-top: 8px;" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="logo ocultar">
                        <h4 class="text-light font-weight-bold mb-0">
                        @hasanyrole('super-admin|admin')
                            <a href="{{ route('admin.index') }}" class="navbar-brand">TestSystem</a>
                        @endhasanyrole
                        </h4> 
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        @auth
                            <h5 class="text-uppercase text-nowrap centrar" style="margin-top: 15px;">
                                <i class="fas fa-house-user"></i>
                                {{ auth()->user()->name ." - ".auth()->user()->lastname}}
                            </h5>
                        @endauth
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                            @guest
                                <li class="nav-item nav-pills efecto">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item efecto">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item ocultarCerrarSesion">
                                    <a class="nav-link text-dark" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                    data-toggle="tooltip" data-placement="left" title="Cerrar sesión">
                                        <i class="fas fa-sign-out-alt fa-2x"></i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
                </nav>
                <!-- Fin Navbar -->
                <!-- Inicia contenido de pagina -->
                <div id="content" class="bg-grey w-100">
                    @yield('content')
                </div>
                <!-- Fin contenido de pagina -->
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    @yield('scripts')
</body>
</html>
