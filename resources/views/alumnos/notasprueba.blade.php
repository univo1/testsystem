<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ModuloV</title>
    <!-- Scripts 
    <script src="{{ asset('js/app.js') }}" defer></script> -->
    <!-- Bootstrap CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> 
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,700&display=swap" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/c7999f3602.js" crossorigin="anonymous"></script>
    <!-- Sweetalert2 Styles -->
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>
      <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
          <a class="navbar-brand" href="#">StudyTest</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        
          <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <a class="nav-link" href="{{ asset('/alumnos') }}">
                  <i class="fas fa-align-justify"></i>
                  Pruebas
                </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">
                    <i class="far fa-bell"></i>
                    Notificaciones
                  </a>
              </li>
              <li class="nav-item">
                  <div class="dropdown">
                      <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-user"></i>
                        Mi cuenta
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">
                          <i class="far fa-address-card"></i>
                          Perfil
                        </button>
                        <button class="dropdown-item" type="button">
                          <i class="fas fa-book"></i>
                          Calificaciones
                        </button>
                        <button class="dropdown-item" type="button">
                          <i class="fas fa-sign-out-alt"></i>
                          Cerrar sesión
                        </button>
                      </div>
                  </div>    
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Buscar">
              <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </form>
          </div>
      </nav>
        <div id="contenido" class="container bg-grey w-100">
           <table class="table mt-5">
  <thead class="table-info">
    <tr>
      <th scope="col">Nombre de test</th>
      <th scope="col">Codigo</th>
      <th scope="col">Fecha</th>
      <th scope="col">Nota</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Ciencia basica</th>
      <td>cie10021635</td>
      <td>10/10/2020 14:27</td>
      <td>8.0</td>
    </tr>
    <tr>
      <th scope="row" colspan="4">No hay mas calificaiones que mostrar</th>
    </tr>
  </tbody>
</table>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light fixed-bottom justify-content-center" style="background-color: #c7e3f7;">
            Copyright 
            &nbsp <i class="fa fa-copyright" aria-hidden="true"></i>&nbsp 
            ModuloV
        </nav>
      </div>
      <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
      <script src="{{ asset('js/popper.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/vue.js') }}"></script>
      <script src="{{ asset('js/axios.js') }}"></script>
      @yield('scripts')
</body>
</html>
