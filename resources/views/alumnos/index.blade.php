<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ModuloV</title>
    <!-- Scripts 
    <script src="{{ asset('js/app.js') }}" defer></script> -->
    <!-- Bootstrap CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> 
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,700&display=swap" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/c7999f3602.js" crossorigin="anonymous"></script>
    <!-- Sweetalert2 Styles -->
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>
      <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
          <a class="navbar-brand" href="#">StudyTest</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        
          <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <a class="nav-link" href="{{ asset('/alumnos') }}">
                  <i class="fas fa-align-justify"></i>
                  Pruebas
                </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ asset('/notificacion') }}">
                    <i class="far fa-bell"></i>
                    Notificaciones
                  </a>
              </li>
              <li class="nav-item">
                  <div class="dropdown">
                      <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-user"></i>
                        Mi cuenta
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a href="{{ asset('/perfil') }}" class="dropdown-item" type="button">
                          <i class="far fa-address-card"></i>
                          Perfil
                        </a>
                        <a href="{{ asset('/notasprueba') }}" class="dropdown-item" type="button">
                          <i class="fas fa-book"></i>
                          Calificaciones
                        </a>
                        <a href="" class="dropdown-item" type="button">
                          <i class="fas fa-sign-out-alt"></i>
                          Cerrar sesión
                        </a>
                      </div>
                  </div>    
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Buscar">
              <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </form>
          </div>
      </nav>
        <div id="contenido" class="container bg-grey w-100 ">
          <h5 class="mt-5 font-italic" style="color:rgba(53, 72, 179, 0.658)">¡BIENVENIDO A TESTSYSTEM!</h5>
            <div class="row">
                <div class="col-sm-6 col-xs-12 pt-5">
                  <div class="card shadow-sm" style="border-color:#a4c1d6">
                    <div class="card-body">
                      <h5 class="card-title">Prueba de CIENCIA BASICA</h5>
                      <p class="card-text">Tiene (2) intento para realizar la prueba</p>
                      <h6 class="font-weight-bold">Finaliza: 11/10/2020 20:35</h6>
                      <a href="#" class="btn text-uppercase float-right" data-toggle="modal" data-target="#codigoModal" style="background-color: #a4c1d6">Comenzar</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal fade" style="margin-top: 200px;" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">StudyTest</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">Empezar la evaluación</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <a href="{{ asset('/preguntauno') }}" class="btn btn-primary">Comenzar intento</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal fade" style="margin-top: 200px;" id="codigoModal" tabindex="-1" aria-labelledby="codigoModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="codigoModalLabel">StudyTest</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <form>
                          <div class="form-group row">
                            <label for="codigo" class="col-sm-4 col-form-label">Código de acceso:</label>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="codigo">
                            </div>
                          </div>
                    </div>
                    <div class="modal-footer">
                      <a href="" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">Ingresar</a>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light fixed-bottom justify-content-center" style="background-color: #c7e3f7;">
          Copyright 
          &nbsp <i class="fa fa-copyright" aria-hidden="true"></i>&nbsp 
          ModuloV
        </nav>
      </div>
      <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
      <script src="{{ asset('js/popper.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/vue.js') }}"></script>
      <script src="{{ asset('js/axios.js') }}"></script>
      @yield('scripts')
</body>
</html>
