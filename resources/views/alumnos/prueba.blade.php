<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ModuloV</title>
    <!-- Scripts 
    <script src="{{ asset('js/app.js') }}" defer></script> -->
    <!-- Bootstrap CSS-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> 
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,700&display=swap" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/c7999f3602.js" crossorigin="anonymous"></script>
    <!-- Sweetalert2 Styles -->
    <link href="{{ asset('sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>
      <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
            <a class="navbar-brand" href="#">StudyTest</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item">
                  <a class="nav-link" href="{{ asset('/alumnos') }}">
                    <i class="fas fa-align-justify"></i>
                    Pruebas
                  </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                      <i class="far fa-bell"></i>
                      Notificaciones
                    </a>
                </li>
                <li class="nav-item">
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="far fa-user"></i>
                          Mi cuenta
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                          <button class="dropdown-item" type="button">
                            <i class="far fa-address-card"></i>
                            Perfil
                          </button>
                          <button class="dropdown-item" type="button">
                            <i class="fas fa-book"></i>
                            Calificaciones
                          </button>
                          <button class="dropdown-item" type="button">
                            <i class="fas fa-sign-out-alt"></i>
                            Cerrar sesión
                          </button>
                        </div>
                    </div>    
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">
                  <i class="fas fa-search"></i>
                </button>
              </form>
            </div>
        </nav>
        <div id="contenido" class="container bg-grey w-100">
            <div class="card text-center mt-5">
                <div class="card-header">
                  Prueba de CIENCIA BÁSICA
                </div>
                <div class="card-body">
                    <h6 class="card-text font-weight-bold">
                      <div class="row">
                        <div class="col-sm-9">
                          <p>
                            Resolución de la evaluación - CÓDIGO: cie10021635 <br>
                            Alumno: Eduardo Vasquez
                          </p>
                        </div>
                        <div class="col-sm-3">
                          CALIFICACIÓN: 8 / 10
                        </div>
                      </div>
                    </h6><br>
                    <ul class="list-group text-justify">
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9 col-xs-12">
                            1. ¿Cómo se llama la teoría que considera que todos los organismos descendemos del mismo ancestro?
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              Celula 
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            2. ¿El proceso mediante el cual se generan moléculas orgánicas a partir de sustancias inorgánicas usando como fuente de energía el sol es?
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              Metabolismo 
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            3. ¿La  columna más a la derecha de la tabla periódica esta compuesta por gases nobles? 
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              False 
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            4. ¿Un microgramo equivale a 0,001 gramos?  
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              False 
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            5. ¿La velocidad a la que viaja la luz es?
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-danger" role="alert">
                              300,000 m/s 
                              <i class="fas fa-times"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                          6. ¿Las partículas subatómicas con carga eléctrica negativa son llamadas Protones?
                        </div>
                        <div class="col-sm-3 col-xs-12 text-center">
                          <div class="alert alert-success" role="alert">
                            False 
                            <i class="fas fa-check"></i>
                          </div>
                        </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                          7. ¿Qué inventó Alfred Nobel, el que da nombre a los famosos premios?
                        </div>
                        <div class="col-sm-3 col-xs-12 text-center">
                          <div class="alert alert-danger" role="alert">
                            Penicilina   
                            <i class="fas fa-times"></i>
                          </div>
                        </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            8. ¿La principal función de los globulos rojos es combatir enfermedades?
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              False   
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            9. ¿Para los botánicos, el tomate es una?
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              Fruta   
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-sm-9">
                            10. ¿El tipo de radiación Ultravioleta te produce quemaduras?
                          </div>
                          <div class="col-sm-3 col-xs-12 text-center">
                            <div class="alert alert-success" role="alert">
                              False   
                              <i class="fas fa-check"></i>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  <a href="#" class="btn btn-primary float-right mt-5">Listo</a>
                </div>
                <div class="card-footer text-muted">
                  
                </div>
              </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light fixed-bottom justify-content-center" style="background-color: #c7e3f7;">
          Copyright 
          &nbsp <i class="fa fa-copyright" aria-hidden="true"></i>&nbsp 
          ModuloV
        </nav>
      </div>
      <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
      <script src="{{ asset('js/popper.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/vue.js') }}"></script>
      <script src="{{ asset('js/axios.js') }}"></script>
      @yield('scripts')
</body>
</html>
