@extends('layouts.form')
@section('form')
<div class="row justify-content-around">
    <div class="col-xs-12 col-md-10">
        <div class="card text-center">
            <div class="card-header">
                <div class="row">
                    <div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
                        {{$title}}
                    </div>
                    <div class="col-2">
                        <a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
                            data-placement="right" title="Cerrar formulario">
                            <i class="far fa-times-circle fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" id="formAddTest">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="col-md-6 mb-3">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name" v-model="name">
                            @error('name')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="codigo">Código</label>
                            <input type="text" class="form-control" id="codigo" :hola="code = name | createCode" :value="code"  name="codigo" readonly>
                            @error('codigo')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div> 
                        <div class="col-md-6 mb-3">
                            <label for="fecha_inicio">Fecha de inicio</label>
                            <input type="date" class="form-control" id="fecha_inicio" value="{{ old('fecha_inicio') }}" v-model="fechainicio" name="fecha_inicio">
                            @error('fecha_inicio')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="fecha_fin">Fecha fin</label>
                            <input type="date" class="form-control" id="fecha_fin" value="{{ old('fecha_fin') }}"
                                name="fecha_fin" v-model="fechafin" >
                            @error('fecha_fin')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div> 
                    </div>
                    <div class="form-row justify-content-around" v-for="(input, index) in inputs" :key="index">
                        <div class="col-md-5">
                            <label for="pregunta">pregunta #@{{index+1}}</label>
                            <input type="text" class="form-control" v-model="input.pregunta" id="pregunta" value="{{ old('pregunta') }}" name="pregunta">
                            @error('pregunta')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-1">
                            <label for="porcentaje">Porcentaje</label>
                            <input type="number" class="form-control" v-model="input.porcen" id="porcentaje" value="{{ old('porcentaje') }}" name="porcentaje">
                            @error('pregunta')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            <label for="tipo_pregunta">Tipo</label>
                            <div class="form-group">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input id="espera" value="1" :name="index" v-model="input.tipo" type="radio"/>Multiple
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input id="activo" value="0" :name="index" v-model="input.tipo" type="radio"/>True/False
                                    </label>
                                </div> 
                            </div>
                            @error('tipo')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-1">
                            <label for="tipo_pregunta">Respuestas</label>
                            <a @click="modaltipo = input.tipo; modalposi = index; $emit('click', mostrarmodal());" data-target="#posicionpregunta"><i class="fas fa-plus-circle"></i></a>
                        </div>
                        <div class="col-md-1">
                            <label for="">Options</label>                            
                                <a @click="remove(index)" v-show="index || ( !index && inputs.length > 1)"><i class="fas fa-minus-circle"></i></a>
                                <a @click="add(index)" v-show="index == inputs.length-1"><i class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                    <!-- Cambiar hasta aqui lo demas es igual -->
                    <div class="card-footer text-muted">
                        <a class="btn btn-primary" @click="envio()" >Guardar</a>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Creación de Respuestas</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div v-if="modaltipo==1" v-for="(multiples, index) in inputs[modalposi].multiples" class="form-row justify-content-around">
                                    <div class="col-md-5">
                                        <label for="pregunta">Respuesta #@{{index+1}}</label>
                                        <input type="text" name="" id="" class="form-control" v-model="multiples.resp">
                                        @error('pregunta')
                                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                                role="alert">
                                                <strong>{{ $message }}</strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2">
                                        <label for="tipo_pregunta">Correcta</label>
                                        <div class="form-group">
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input id="espera" value="1" :name="'restp-1-'+index" v-model="multiples.tipo" type="radio"/>Si
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input id="activo" value="0" :name="'restp-1-'+index" v-model="multiples.tipo" type="radio"/>No
                                                </label>
                                            </div> 
                                        </div>
                                        @error('tipo')
                                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                                role="alert">
                                                <strong>{{ $message }}</strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="col-md-1">
                                        <label for="">Options</label>                            
                                            <a @click="removeliteral(modalposi,index)" v-show="index || ( !index && inputs[modalposi].multiples.length > 1)"><i class="fas fa-minus-circle"></i></a>
                                            <a @click="addliteral(modalposi)" v-show="index == inputs[modalposi].multiples.length-1"><i class="fas fa-plus-circle"></i></a>
                                    </div>
                                </div>
                                <div v-if="modaltipo==0" v-for="(vf, index) in inputs[modalposi].vf">
                                    <div class="form-group">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input id="espera" :value="vf.correcta" v-model="vf.correcta" :name="'restp-0-'+modalposi" @click="correcta(modalposi,index);" type="radio"/>@{{vf.resp}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
@endsection
@section('scripts-list')
<script src="{{ asset('sweetalert2/sweetalert2.min.js')}}"></script>
<script>
    new Vue({
        el:'#formAddTest',
        data:{
            modaltipo:0,
            modalposi:0,
            name : '',
            code: '',
            fechafin: '',
            fechainicio: '',
            counter : 1,
            inputs: [ {pregunta:'',
                        multiples:[{resp:'',correcta:0}],
                        vf:[
                            {resp:'Verdadero',correcta:0},
                            {resp:'Falso',correcta:0}
                        ],
                        tipo:1,
                        porcen:0
                    } ]
        },
        filters: {
            createCode()
            {
                let date = new Date(),
                    currentDateWithFormat = date.getUTCDate()+''+(date.getUTCMonth()+1)+''+date.getHours()+date.getMinutes()
                return this.name.toUpperCase().substring(0, 3)+currentDateWithFormat+Math.floor(Math.random() * 99);
            }
        },
        methods: {
            add(index) {
                this.inputs.push({ pregunta:'', multiples:[{resp:'',correcta:0}],
                    vf:[ {resp:'Verdadero',correcta:0}, {resp:'Falso',correcta:0} ],
                    tipo:1,
                    porcen:0
                });
                console.log(this.inputs);
                this.counter++
            },
            remove(index) {
                this.inputs.splice(index, 1);
            },
            addtextpregunta(index,pregunta){
                this.inputs[index]['pregunta'] = pregunta;
            },
            addliteral(indexpregunta){
                console.log(indexpregunta)
                this.inputs[indexpregunta].multiples.push({resp:'',correcta:false});
            },
            removeliteral(indexpregunta,indexliteral) {
                this.inputs[indexpregunta].multiples.splice(indexliteral, 1);
            },
            correcta(posipreguntas,posirespuestas){
                //console.log(this.inputs[posipreguntas].vf);                
                this.inputs[posipreguntas].vf.forEach(function(multi){
                    multi.correcta = 0;
                });
                this.inputs[posipreguntas].vf[posirespuestas].correcta = 1;
                console.log(this.inputs);
            },
            envio:function() {
                var url = "{{ route('test.store') }}";
                var params = {
                    _token  : this.tk,
                    name: this.name,
                    code: this.code,
                    fechainicio: this.fechainicio,
                    fechafin: this.fechafin,
                    preguntas: this.inputs

                };
                this.api(url, params);
            },
            alertaa: function(message = 'Test creado sastifactoriamente', title = '¡Test Guardado con exito!',icon = 'success') {
                Swal.fire({
                    position: 'center',
                    icon: icon,
                    title: title,
                    text: message,
                    showConfirmButton: true,
                    confirmButtonColor: '#ffa726',
                    confirmButtonText: '¡Ok, entiendo!'
                }).then((result) => { location.href ="{{ route('test.index') }}"; })

			},
            api:function(url, param){
                axios.post(url, param)
                .then(response => {
                    this.alertaa();
                }).catch(e => {
                    console.log(e);
                });
            }
        }
    });
    function mostrarmodal(){
        $('#exampleModal').modal('toggle');
    }
</script>
@endsection