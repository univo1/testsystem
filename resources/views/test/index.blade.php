@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
    <div class="row">
      <div class="col-auto">
        <h5 class="card-title text-uppercase font-weight-bold">
          <i class="fas fa-align-justify"></i>
          {{$title}}
        </h5>
      </div>
    </div>
	<table class="table table-borderless table-hover  table-responsive-lg">
  <thead>
      <tr class="text-uppercase font-italic">
        <th scope="col">Nombre</th>
        <th scope="col">Codigo</th>
        <th scope="col">Fecha Inicio</th>
        <th scope="col">Fecha Fin</th>
        @can($table.'.update')
        <th scope="col">Editar</th>
        @endcan
        @can($table.'.delete')
        <th scope="col">Eliminar</th>
        @endcan
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <td scope="row">{{ $e->name }}</td>
        <td scope="row">{{ $e->codigo }}</td>
        <td scope="row">{{ $e->fecha_inicio }}</td>
        <td scope="row">{{ $e->fecha_fin }}</td>
        @can($table.'.update')
        <td>
          <a href="{{ route($table.'.edit', ['test' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
            <i class="fas fa-pen"></i>
          </a>
        </td>
        @endcan
        @can($table.'.delete')
        <td>
          <form action="{{ route($table.'.destroy', ['test' => $e->id ]) }}" method="post" class="frmDelete">
            @csrf
            @method('DELETE')
            <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
              <i class="fas fa-eraser"></i>
            </button>
          </form>
        </td>
        @endcan
      </tr>
      @endforeach
    </tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection
