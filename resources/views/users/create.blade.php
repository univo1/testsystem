@extends('layouts.form')
@section('form')
<div class="row justify-content-around">
    <div class="col-xs-12 col-md-6">
        <div class="card text-center">
            <div class="card-header">
                <div class="row">
                    <div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
                        {{$title}}
                    </div>
                    <div class="col-2">
                        <a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
                            data-placement="right" title="Cerrar formulario">
                            <i class="far fa-times-circle fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-row justify-content-around">
                        <div class="col-md-6 mb-3">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name">
                            @error('name')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastname">Apellido</label>
                            <input type="text" class="form-control" id="lastname" value="{{ old('lastname') }}" name="lastname">
                            @error('lastname')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email">Correo</label>
                            <input type="text" class="form-control" id="email" value="{{ old('email') }}" name="email">
                            @error('email')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="telefono">teléfono</label>
                            <input type="text" class="form-control" id="telefono" value="{{ old('telefono') }}" name="telefono">
                            <small>Número de telefono sin área, ni guiones. Ej.: 77777777</small>
                            @error('telefono')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="dui">Dui</label>
                            <input type="text" class="form-control" id="dui" value="{{ old('dui') }}" name="dui">
                            @error('dui')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="tipo_usuario">Tipo de Usuario</label>
                            <select class="form-control" name="tipo_usuario" id="tipo_usuario">
								<option value="">Seleccione un tipo</option>
                                <option value="711">Administrador</option>
                                <option value="640">Docente</option>
                                <option value="644">Alumno</option>
							</select>
                            @error('tipo_usuario')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="role">Rol del usuario</label>
                            <select class="form-control" name="role" id="role">
								<option value="">Seleccione un Rol para el usuario</option>
                                @foreach ($role as $e)
									<option value="{{ $e->id }}">{{ $e->name }}</option>
								@endforeach
							</select>
                            @error('role')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" value="{{ old('password') }}" name="password">
                            @error('password')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <!-- Cambiar hasta aqui lo demas es igual -->
                    <div class="card-footer text-muted">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
@endsection
