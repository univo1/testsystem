@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
    <div class="row">
      <div class="col-auto">
        <h5 class="card-title text-uppercase font-weight-bold">
          <i class="fas fa-align-justify"></i>
          {{$title}}
        </h5>
      </div>
    </div>
	<table class="table table-borderless table-hover  table-responsive-lg">
  <thead>
      <tr class="text-uppercase font-italic">
        <th scope="col">Nombre</th>
        <th scope="col">Apellido</th>
        <th scope="col">Correo</th>
        <th scope="col"># Telefono</th>
        @can($table.'.roles')
        <th scope="col">Roles</th>
        @endcan
        <th scope="col">Dui</th>
        @can($table.'.update')
        <th scope="col">Editar</th>
        @endcan
        @can($table.'.delete')
        <th scope="col">Eliminar</th>
        @endcan
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <td scope="row">{{ $e->name }}</td>
        <td scope="row">{{ $e->lastname }}</td>
        <td scope="row">{{ $e->email }}</td>
        <td scope="row">{{ $e->telefono }}</td>
        @can($table.'.roles')
        <td scope="row"> 
          @if($e->role_id)
            @foreach($e->roles as $roles)
              {{ $roles->name }}
            @endforeach
          @else 
            N/A
          @endif          
        </td>
        @endcan
        <td scope="row">{{ $e->dui }}</td>
        @can($table.'.update')
        <td>
          <a href="{{ route($table.'.edit', ['user' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
            <i class="fas fa-pen"></i>
          </a>
        </td>
        @endcan
        @can($table.'.delete')
        <td>
          @if(auth()->user()->id != $e->id)
            <form action="{{ route($table.'.destroy', ['user' => $e->id ]) }}" method="post" class="frmDelete">
              @csrf
              @method('DELETE')
              <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
                <i class="fas fa-eraser"></i>
              </button>
            </form>
          @endif
        </td>
        @endcan
      </tr>
      @endforeach
    </tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection
