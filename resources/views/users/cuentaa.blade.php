@extends('layouts.cliente')
@section('contenido')
<div class="row justify-content-around" style="margin-top: 150px">
    <div class="col-xs-12 col-md-6">
        <div class="card text-center">
            <div class="card-header">
                <div class="row">
                    <div class="col-12 text-uppercase font-weight-bold font-italic centrar-letras">
                        {{$title}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('users.updatecuenta', ['user' => auth()->user()->id]) }}" method="POST"
                    accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-around">
                        <div class="col-md-6 mb-3">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" value="{{ $data->name }}" name="name">
                            @error('name')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastname">Apellido</label>
                            <input type="text" class="form-control" id="lastname" value="{{ $data->lastname }}" name="lastname">
                            @error('lastname')
                                <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                    role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email">Correo</label>
                            <input type="text" class="form-control" id="email" value="{{ $data->email }}" name="email">
                            @error('email')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="telefono">teléfono</label>
                            <input type="text" class="form-control" id="telefono" value="{{ $data->telefono }}" name="telefono">
                            <small>Número de telefono sin área, ni guiones. Ej.: 77777777</small>
                            @error('telefono')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="dui">Dui</label>
                            <input type="text" class="form-control" id="dui" value="{{ $data->dui }}" name="dui">
                            @error('dui')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>                    
                        <div class="col-md-12 mb-3">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" value="{{ old('password') }}" name="password">
                            @error('password')
                            <div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
                                role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <!-- Cambiar hasta aqui lo demas es igual -->
                    <div class="card-footer text-muted">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
@endsection