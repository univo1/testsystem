@extends('layouts.app')
@section('content')
   <div class="container">
    <div class="row center-align">
        @foreach ($modulos as $m)
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card p-4 w-75 mx-auto">
                    <div class="card-header">
                        <span class="card-title upper">{{ (isset($m['title']) && $m['title'] != '')?$m['title']:$m['name'] }}</span>
                    </div>
                    <div class="card-body">
                    
                        <a href="{{ route($m['name'].'.index') }}">Ver</a>
                        <a href="{{ route($m['name'].'.create') }}">Agregar</a>
                    </div>
                </div><br>
            </div>
        @endforeach
      </div>
   </div>
@endsection 