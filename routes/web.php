<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/notasprueba', function () {
    return view('alumnos.notasprueba');
});
Route::get('/notificacion', function () {
    return view('alumnos.notificacion');
});
Route::get('/perfil', function () {
    return view('alumnos.perfil');
});
Route::get('/alumnos', function () {
    return view('alumnos.index');
})->name('alumnos.index');
Route::get('/prueba', function () {
    return view('alumnos.prueba');
});
Route::get('/preguntauno', function () {
    return view('cienciabasica.preguntauno');
});
Route::get('/preguntados', function () {
    return view('cienciabasica.preguntados');
});
Route::get('/preguntatres', function () {
    return view('cienciabasica.preguntatres');
});
Route::get('/preguntacuatro', function () {
    return view('cienciabasica.preguntacuatro');
});
Route::get('/preguntacinco', function () {
    return view('cienciabasica.preguntacinco');
});
Route::get('/preguntaseis', function () {
    return view('cienciabasica.preguntaseis');
});
Route::get('/preguntasiete', function () {
    return view('cienciabasica.preguntasiete');
});
Route::get('/preguntaocho', function () {
    return view('cienciabasica.preguntaocho');
});
Route::get('/preguntanueve', function () {
    return view('cienciabasica.preguntanueve');
});
Route::get('/preguntadiez', function () {
    return view('cienciabasica.preguntadiez');
});
Auth::routes();
Route::get('verify/{code}','Auth\RegisterController@verifyUser')->name('verify.user');

Route::middleware(['auth'])->group(function(){

    //Route::group(['middleware' => ['role:admin|usuario|manager|dependiente']], function () {
        //rutas de administracion
        Route::get('/home', 'HomeController@index')->name('home');
            
        //index de administracion
        Route::get('admin', 'AdminController@index')
                ->name('admin.index');
        
    
        //ruta de Roles
        Route::resource('roles','RolesController', ['except'=>'show']);
        Route::post('roles/search','RolesController@search')
            ->name('roles.search');
    
        //ruta de Permisos
        Route::resource('permission','PermissionController', ['except'=>'show']);
        Route::post('permission/search','PermissionController@search')
            ->name('permission.search');
    
        //ruta de Usuarios
        Route::resource('users','UserController', ['except'=>'show']);
        Route::post('users/search','UserController@search')
            ->name('users.search');

        //ruta de tests
        Route::resource('test','TestController', ['except'=>'show']);
        Route::post('test/search','TestController@search')
            ->name('test.search');
    //});
});


