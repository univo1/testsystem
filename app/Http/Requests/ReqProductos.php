<?php

namespace App\Http\Requests;

use App\Categorias;
use Illuminate\Foundation\Http\FormRequest;

class ReqProductos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoria_id' => 'required|integer',
            'producto' => 'required|max:80'
        ];
    }

    public function messages()
    {
        return [
            'categoria_id.required' => 'Es nesesario seleccionar una :attribute',
            'categoria_id.integer' => 'El :attribute tinie que ser entero',
            'producto.required' => 'El :attribute es obligatorio',
            'producto.max' => 'El Nombre del :attribute no tiene que tener una cantidad mayor de 80 caracteres'
        ];
    }

    public function attributes()
    {
        return [
            'categoria_id' => 'Categoria',
            'producto' => 'Producto'
        ];
    }
}
