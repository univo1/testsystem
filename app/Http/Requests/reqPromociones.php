<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class reqPromociones extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'titulo'          => 'required|max:255',
            'precio_regular'  => 'required|numeric|gte:0|between:0.0000,9999999999.9999',
            'precio_oferta'   => 'required|numeric|gte:0|between:0.0000,9999999999.9999',
            'fecha_inicio'    => 'required',
            'fecha_fin'       => 'required',
            'fecha_limite'    => 'required',
            'descripcion'     => 'required|max:700',
            'limite'          => 'required',
            'cantidad'        => 'required_if:limite,==,2|numeric|gte:0',
        ];
        
        if(Request::isMethod('PUT')) 
            $rules += ['estado' => ['numeric']];

        return $rules;
    }

    public function messages()
    {
        return [
            'titulo.required'          => 'El campo Titulo es requeredo',
            'titulo.max'               => 'La longuitud maxima para el campo Titulo es de 255',
            'precio_regular.required'  => 'El campo Precio Regular es requerido',
            'precio_regular.numeric'   => 'El campo Precio Regular debe de ser numérico',
            'precio_regular.gte'       => 'En el campo Precio Regular no se aceptan numéros negativos',
            'precio_regular.between'   => 'El campo Precio Regular debe de comprender entre 0 a 9,999,999.9999 digite una cantidad valida',
            'precio_oferta.required'   => 'El campo Precio Oferta es requerido',
            'precio_oferta.numeric'    => 'El campo Precio Oferta debe de ser numérico',
            'precio_oferta.gte'        => 'En el campo Precio Oferta no se aceptan numéros negativos',
            'precio_oferta.between'    => 'El campo Precio Oferta debe de comprender entre 0 a 9,999,999.9999 digite una cantidad valida',
            'fecha_inicio.required'    => 'El :attribute es requerido',
            'fecha_inicio.date_format' => 'El formato :attribute es incorrecto',
            'fecha_fin.required'       => 'El :attribute es requerido',
            'fecha_fin.date_format'    => 'El formato :attribute es incorrecto',
            'fecha_limite.required'    => 'El :attribute es requerido',
            'fecha_limite.date_format' => 'El formato :attribute es incorrecto',
            'descripcion.required'     => 'El campo Descripcion es requeredo',
            'descripcion.max'          => 'La longuitud maxima para el campo Descripcion es de 700',
            'estado.required'          => 'El campo Estado es requeredo',
            'estado.numeric'           => 'El campo Estado debe de ser numérico',
            'cantidad.numeric'         => 'El campo Cantidad debe de ser numérico',
            'cantidad.gte'             => 'En el campo Cantidad no se aceptan numéros negativos',
            'cantidad.required_if'     => 'El campo Cantidad es requeredo',
        ];
    }

}
