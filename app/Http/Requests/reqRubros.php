<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqRubros extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        $rules = [
            'rubro' => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:75',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'rubro.required' => 'El :attribute es requerido',
            'rubro.regex' => 'El :attribute no debde de contener carácteres especiales ni números',
            'rubro.max' => 'El :attribute no debe de contener más de 75 carácteres'            
        ];
    }

    public function attributes()
    {
        return [
            'rubro' => 'Rubro',
        ];
    }
}
