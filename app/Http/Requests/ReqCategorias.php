<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReqCategorias extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'categoria' => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:75',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'categoria.required' => 'El :attribute es requerido',
            'categoria.regex' => 'El :attribute no debde de contener carácteres especiales ni números',
            'categoria.max' => 'El :attribute no debe de contener más de 75 carácteres'            
        ];
    }

    public function attributes()
    {
        return [
            'categoria' => 'Categoria',
        ];
    }
}
