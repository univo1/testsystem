<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ReqEmpresas extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'rubro'          => 'required_if:empresa_id,==,0',
            'codigo'         => 'required|alpha_num|max:75',
            'nombre_empresa' => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:75',
            'direccion'      => 'required',
            'descripcion'    => 'required|min:8|max:500',
            'email'          => 'required|regex:/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/',
            'comision'       => 'required_if:empresa_id,==,0|numeric|gte:10'
        ];
        if (Request::isMethod('POST'))
            $rules += ['empresa_id' => ['required','numeric']];
        
        else if(Request::isMethod('PUT')) 
            $rules += ['empresa_id' => ['numeric']];
        return $rules;
    }

    public function messages()
    {
        return [
            'rubro.required_if'       => 'Campo requeredo',
            'codigo.required'         => 'Campo codigo es requerido',
            'codigo.alpha_num'        => 'Campo codigo no permite carcteres especiales',
            'codigo.max'              => 'El codigo no debe de contener más de 75 carácteres',
            'nombre_empresa.required' => 'Campo nombre de la empresa es requerido',
            'nombre_empresa.regex'    => 'Campo nombre de la empresa no se permiten carcteres especiales',
            'nombre_empresa.max'      => 'El nombre de la empresa no debe de contener más de 75 carácteres',
            'direccion.required'      => 'Campo dirección es requerido',
            'descripcion.required'    => 'Campo direccion es requerido',
            'descripcion.min'         => 'Caracteres minimos para el campo descripcion es de 8',
            'descripcion.max'         => 'Caracteres maximos para el campo descripcion es de 500',
            'email.required'          => 'Campo email es requerido',
            'email.regex'             => 'Ingrese un email valido',
            'comision.required_if'    => 'Campo comision es requerido',
            'comision.numeric'        => 'Campo comision es numerico',
            'comision.gte'            => 'Comision minima tiene que ser del 10%',
        ];
    }
}
