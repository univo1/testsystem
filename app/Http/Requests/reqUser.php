<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'         => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:75',
            'lastname'     => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:75',
            'email'        => 'required|email|unique:users,email,'.$this->user,
            'telefono'     => 'required|numeric|unique:users,telefono,'.$this->user,
            'dui'          => 'required|numeric|unique:users,dui,'.$this->user,
            'tipo_usuario' => 'required|integer',
            'password'     => 'required|min:8',
            'role'         => 'required|integer'
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'         => 'El :attribute es requerido',
            'name.regex'            => 'El :attribute no debe de contener carácteres especiales ni números',
            'name.max'              => 'El :attribute no debe de contener más de 75 carácteres',
            'lastname.required'     => 'El :attribute es requerido',
            'lastname.regex'        => 'El :attribute no debe de contener carácteres especiales ni números',
            'lastname.max'          => 'El :attribute no debe de contener más de 75 carácteres',
            'email.required'        => 'El :attribute es requerido',
            'email.email'           => 'El :attribute no es de tipo email',
            'email.unique'          => 'Ya existe un :attribute registrado, porfavos ingrese uno nuevo',
            'telefono.required'     => 'El :attribute es requerido',
            'telefono.numeric'      => 'El :attribute debe de ser numeros',
            'telefono.unique'       => 'Ya existe un :attribute registrado, porfavos ingrese uno nuevo',
            'dui.required'          => 'El :attribute es requerido',
            'dui.numeric'           => 'El :attribute debe de ser numeros',
            'dui.unique'            => 'Ya existe un :attribute registrado, porfavos ingrese uno nuevo',
            'tipo_usuario.required' => 'El :attribute es requerido',
            'tipo_usuario.integer'  => 'No es permitido alterar el :attribute ',
            'password.required'     => 'La :attribute es requerida',
            'password.min'          => 'La :attribute debe ser superior a 8',
            'role.required'         => 'El :attribute es requerido',
            'role.integer'          => 'No es permitido alterar el campo :attribute ',
        ];
    }

    public function attributes()
    {
        return [
            'name'         => 'Nombre',
            'lastname'     => 'Apellido',
            'email'        => 'Correo',
            'telefono'     => 'Teléfono',
            'dui'          => 'Dui',
            'tipo_usuario' => 'Tipo de usuario',
            'password'     => 'Contraseña',
            'role'         => 'Rol de Usuario' 
        ];
    }
}
