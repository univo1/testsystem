<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqRoles extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/|max:75',
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'El :attribute es requerido',
            'name.regex' => 'El :attribute no debde de contener carácteres especiales ni números',
            'name.max' => 'El :attribute no debe de contener más de 75 carácteres'            
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
        ];
    }
}
