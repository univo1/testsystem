<?php

namespace App\Http\Controllers;

use App\notas_test;
use Illuminate\Http\Request;

class NotasTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\notas_test  $notas_test
     * @return \Illuminate\Http\Response
     */
    public function show(notas_test $notas_test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\notas_test  $notas_test
     * @return \Illuminate\Http\Response
     */
    public function edit(notas_test $notas_test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\notas_test  $notas_test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, notas_test $notas_test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\notas_test  $notas_test
     * @return \Illuminate\Http\Response
     */
    public function destroy(notas_test $notas_test)
    {
        //
    }
}
