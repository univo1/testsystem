<?php

namespace App\Http\Controllers;

use App\ppreguntas;
use Illuminate\Http\Request;

class PpreguntasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ppreguntas  $ppreguntas
     * @return \Illuminate\Http\Response
     */
    public function show(ppreguntas $ppreguntas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ppreguntas  $ppreguntas
     * @return \Illuminate\Http\Response
     */
    public function edit(ppreguntas $ppreguntas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ppreguntas  $ppreguntas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ppreguntas $ppreguntas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ppreguntas  $ppreguntas
     * @return \Illuminate\Http\Response
     */
    public function destroy(ppreguntas $ppreguntas)
    {
        //
    }
}
