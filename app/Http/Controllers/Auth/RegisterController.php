<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->telefono = $request->telefono;
        $user->tipo_usuario = $request->type;
        $user->password = Hash::make($request->password);
        $user->dui = $request->dui;
        $user->verification_code = sha1(time());
        $e = $user->save();

        if($user !=null && $request->empresa_id == 0)
        {
            MailController::sendSingUpEmail($user->name, $user->email, $user->verification_code);
        }

        return redirect()->back()
                ->with(($e)? 'info': 'danger',($e)?
                                                'Tu cuenta a sido creada con exito. Por favor revisa tu correo para activar la cuenta':
                                                'Ocurrio un problema al registrar la cuenta intente de nuevo.');
    }

    public function verifyUser(Request $request)
    {
        $verify_code = $request->code;
        $user = User::where(['verification_code' => $verify_code])->first();

        if($user != null)
        {
            $user->auth = 1;
            $e = $user->save();
        }
        return redirect()
                ->route('login')
                ->with(($e)?'info':'danger',($e)?'Cuenta Activada con exito. !Porfavor inicie session!':'Ocurrio un problema al activar la cuenta intente de nuevo.');
        
    } 
}
