<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            $user = Auth::user();
            switch ($user->tipo_usuario) {
                case 700://super-admin
                    return redirect()->route('admin.index');
                break;
                case 711://admin
                    return redirect()->route('admin.index');
                break;
                case 640: //docente
                    return redirect()->route('test.index');
                break;
                default://estudiante case 644
                    return redirect()->route('alumnos.index');
                break;
            }
        } else return redirect()->route('prueba.home');
    }
    public function list(){
        return view('home');
    }
}
