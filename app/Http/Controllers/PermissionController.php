<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'permission';
    public function index()
    {
        $data = DB::table("permissions")
                    ->select("permissions.*")
                    ->paginate(9);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Lista de Permisos',
            'data'  => $data
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de rubros',
            'data'      => Permission::where('name', 'like', '%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', [
            'table' =>  $this->table, 
            'title'=>'Agregar Permisos'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $permission        = new Permission();
        $permission->name = $request->name;
        $permission->guard_name = 'web';
        $e             = $permission->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar la marca intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit($data)
    {
        $permission = Permission::findOrfail($data);
        return view($this->table.'.edit', ['table' =>  $this->table, 'title'=>'Actualizar Rubros','data'=>$permission]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $data)
    {
        $permission        = Permission::findOrfail($data);
        $permission->name = $request->name;
        $e            = $permission->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el rol intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */

    public function destroy($data)
    {
        $permission = Permission::findOrfail($data);
        $m = $permission->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
