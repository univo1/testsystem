<?php

namespace App\Http\Controllers;

use App\respuestas_pregunta_multiple;
use Illuminate\Http\Request;

class RespuestasPreguntaMultipleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\respuestas_pregunta_multiple  $respuestas_pregunta_multiple
     * @return \Illuminate\Http\Response
     */
    public function show(respuestas_pregunta_multiple $respuestas_pregunta_multiple)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\respuestas_pregunta_multiple  $respuestas_pregunta_multiple
     * @return \Illuminate\Http\Response
     */
    public function edit(respuestas_pregunta_multiple $respuestas_pregunta_multiple)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\respuestas_pregunta_multiple  $respuestas_pregunta_multiple
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, respuestas_pregunta_multiple $respuestas_pregunta_multiple)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\respuestas_pregunta_multiple  $respuestas_pregunta_multiple
     * @return \Illuminate\Http\Response
     */
    public function destroy(respuestas_pregunta_multiple $respuestas_pregunta_multiple)
    {
        //
    }
}
