<?php

namespace App\Http\Controllers;

use App\ppreguntas;
use App\respuestas_pregunta;
use App\test;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'test';
    public function index()
    {
        $user = User::findOrfail(Auth::user()->id);
        if( $user->hasAnyRole('super-admin','admin') )
        {
            $data = DB::table('tests')
                ->select('tests.*')
                ->paginate(7);
        }
        $data = DB::table('tests')
                ->select('tests.*')
                ->where('user_id', '=', Auth::user()->id)
                ->paginate(7);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Lista de Tests',
            'data'  => $data
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de Test',
            'data'      => test::where('name', 'like', '%'.$r->txtSearch.'%')->orwhere('codigo','like','%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', [
            'table' =>  $this->table, 
            'title'=>'Crear Test'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test               = new test();
        $test->user_id      = auth()->id();
        $test->name         = $request->name;
        $test->codigo       = $request->code;
        $test->fecha_inicio = $request->fechainicio;
        $test->fecha_fin    = $request->fechafin;
        $e                  = $test->save();
        if($e != null){
            foreach($request->preguntas as $row){
                $preguntastest = new ppreguntas();
                $preguntastest->test_id = $test->id;
                $preguntastest->pregunta = $row['pregunta'];
                $preguntastest->tipo_pregunta = $row['tipo']+1;
                $preguntastest->porcentaje = $row['porcen'];
                $preguntastest->save();
                if($row['tipo']+1 == 2){
                    foreach($row['multiples'] as $rows){
                        $respuestas = new respuestas_pregunta();
                        $respuestas->pregunta_id = $preguntastest->id;
                        $respuestas->literal = $rows['resp'];
                        $respuestas->correcta = $rows['correcta'];
                        $respuestas->save();
                    }
                }else{
                    foreach($row['vf'] as $rows){
                        $respuestas = new respuestas_pregunta();
                        $respuestas->pregunta_id = $preguntastest->id;
                        $respuestas->literal = $rows['resp'];
                        $respuestas->correcta = $rows['correcta'];
                        $respuestas->save();
                    }
                }
            }
        }
        return $e;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy($data)
    {
        $tests = test::findOrfail($data);
        $m = $tests->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
