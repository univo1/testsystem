<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\reqRoles;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'roles';
    public function index()
    {
        if(Auth::user()->id != 1){

            $data = Role::with('permissions:name')
                ->select('roles.id as id',
                    'roles.name as name',
                    'roles.guard_name as guard_name')
                    ->where('name', '!=', 'super-admin')
                ->paginate(5);
        }
        $data = Role::with('permissions:name')
                ->select('roles.id as id',
                    'roles.name as name',
                    'roles.guard_name as guard_name')
                    
                ->paginate(5);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Lista de Roles',
            'data'  => $data
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de rubros',
            'data'      => Role::where('name', 'like', '%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', [
            'table' =>  $this->table, 
            'title'=>'Agregar Roles',
            'permi' => Permission::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqRoles $request)
    {
        $e = Role::create(['name' => $request->name])
                    ->syncPermissions($request->permisos);

        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar la marca intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function show(Role $roles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $roles, $data)
    {
        $role = $roles::findOrfail($data);
        return view($this->table.'.edit', [
            'table' =>  $this->table, 
            'title'=>'Actualizar Rubros',
            'data'=>$role,
            'permi' => Permission::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function update(reqRoles $request, Role $roles,$data)
    {
        $role       = $roles::findOrfail($data);
        $role->name = $request->name;
        $e          = $role->save();
        $role->syncPermissions($request->permisos);
        
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el rol intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $roles,$data)
    {
        $role = $roles::findOrfail($data);
        $m = $role->delete();
        return redirect()->route($this->table.'.index')
                        ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
