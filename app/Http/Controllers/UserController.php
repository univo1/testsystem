<?php

namespace App\Http\Controllers;

use App\Http\Requests\reqUser;
use App\Roles;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = "users";
    public function index()
    {
        $user = User::with('roles')
                ->select('users.*','model_has_roles.role_id')
                ->leftJoin('model_has_roles','model_has_roles.model_id', '=', 'users.id')
                ->latest()
                ->where('tipo_usuario','!=','700')
                ->paginate(50);

        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Lista de Usuario',
            'data'  => $user
        ]);
    }

    
    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     => 'Listado de Usuarios',
            'txtSearch' => $r->txtSearch,
            'data'      => User::select('id', 'name', 'lastname','email', 'telefono', 'dui','tipo_usuario','role_id')
                                ->leftJoin('model_has_roles','model_has_roles.model_id', '=', 'users.id')
                                ->latest()
                                ->where('name', 'like', '%'.$r->txtSearch.'%')
                                ->paginate(50)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('name', '!=', 'super-admin')->get();

        return view($this->table.'.create', [
            'table' =>  $this->table, 
            'title'=>'Agregar Usuarios',
            'role' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveUser($data)
    {
        $user               = new User();
        $user->name         = $data->name;
        $user->lastname     = $data->lastname;
        $user->email        = $data->email;
        $user->telefono     = $data->telefono;
        $user->dui          = $data->dui;
        $user->tipo_usuario = $data->tipo_usuario;
        $user->auth         = 1;
        $user->password     = Hash::make($data->password);
        $e                  = $user->save();
        
        if ($data->role)
        {
            $user->assignRole($data->role);
        }
        
        return $e;
    }
    
    public function store(reqUser $request)
    {
        $e = $this->saveUser($request);
        return redirect()->route($this->table.'.index')
                ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar al usuario intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($data)
    {
        $users = User::findOrfail($data);
        $role = Roles::all();
        return view($this->table.'.edit', [
            'table' =>  $this->table, 
            'title'=>'Actualizar usuarios',
            'data'=> $users,
            'role' => $role
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function editcuenta($data)
    {
        $users = User::findOrfail($data);
        return view('users.cuentaa', [
            'table' =>  $this->table, 
            'title'=>'Actualizar usuario',
            'data'=> $users
        ]);
    }

    public function cuentaa()
    {
        $user = DB::table("users")
            ->select('users.*')
            ->where('id', '=', auth()->user()->id)
            ->get();
        return view('users.cuentaa', [
            'table' => $this->table,
            'title' => 'Datos de cuenta',
            'data'  => $user,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(reqUser $request, User $users, $data)
    {
        $user               = $users::findOrfail($data);
        $user->name         = $request->name;
        $user->lastname     = $request->lastname;
        $user->email        = $request->email;
        $user->telefono     = $request->telefono;
        $user->dui          = $request->dui;
        $user->tipo_usuario = $request->tipo_usuario;
        $user->password     = Hash::make($request->password);
        $e                  = $user->save();
        $user->syncRoles($request->role);
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el rubro intente de nuevo.');
    
        
        
        }
    /**
     * Updatecuenta the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatecuenta(Request $request, User $users, $data)
    {
        $user               = $users::findOrfail($data);
        $user->name         = $request->name;
        $user->lastname     = $request->lastname;
        $user->email        = $request->email;
        $user->telefono     = $request->telefono;
        $user->dui          = $request->dui;
        $user->password     = Hash::make($request->password);
        $e                  = $user->save();
        return view($this->table.'.cuentaa', [
            'table' =>  $this->table,
            'title' =>'Lista de Usuario',
            'data'  => $user
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($data)
    {
        $users = User::findOrfail($data);
        $m = $users->delete();
        return redirect()->route($this->table.'.index')
                ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
