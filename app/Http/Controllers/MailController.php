<?php

namespace App\Http\Controllers;

use App\Mail\SingUpEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static function sendSingUpEmail($name, $email, $verification_code, $type = 644, $route = 'verify.user')
    {
        $data = [
            'name' => $name,
            'verification_code' => $verification_code,
            'type' => $type,
            'route' => $route
        ];
        Mail::to($email)->send(new SingUpEmail($data));
    }
}
