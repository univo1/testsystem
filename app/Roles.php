<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Roles extends Model
{
    protected $table = 'roles';
    
    public function permissions() : BelongsToMany
    {
        return $this->belongsToMany(Permission::class,'role_has_permissions','role_id','permission_id');
    }
}
